package validator

import (
	"net"
	"strings"
)

func CheckIP(ipAddr string) bool {
	ip := net.ParseIP(ipAddr)
	if ip != nil {
		return true
	}
	return false
}

func CheckIPv4(ipAddr string) bool {
	ip := net.ParseIP(ipAddr)
	if ip == nil {
		return false
	}
	ipv4 := ip.To4()
	if ipv4 != nil &&
		strings.Count(ipAddr, ".") == 3 &&
		!strings.Contains(ipAddr, ":") {
		return true
	}
	return false
}

func CheckIPv6(ipAddr string) bool {
	ip := net.ParseIP(ipAddr)
	if ip == nil {
		return false
	}
	ipv4 := ip.To16()
	if ipv4 != nil &&
		strings.Count(ipAddr, ":") >= 2 {
		return true
	}
	return false
}
