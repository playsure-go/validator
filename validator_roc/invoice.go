package validator_roc

import "regexp"

func CheckMobileBarcode(barcode string) bool {
	re := regexp.MustCompile("^\\/[0-9A-Z\\.\\-+]{7}$")
	return re.MatchString(barcode)
}

func CheckCitizenDigitalCertificate(code string) bool {
	re := regexp.MustCompile("^[A-Z]{2}[0-9]{14}$")
	return re.MatchString(code)
}

func CheckLoveCode(loveCode string) bool {
	re := regexp.MustCompile("^[0-9]{3,7}$")
	return re.MatchString(loveCode)
}
