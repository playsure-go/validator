package validator_roc

import "regexp"

func CheckIdNoSimple(idNo string) bool {
	re := regexp.MustCompile("^[A-Z][1289][0-9]{8}$")
	return re.MatchString(idNo)
}

func CheckIdNoDetail(idNo string) bool {
	if !CheckIdNoSimple(idNo) {
		return false
	}

	/*
	 * idNo     A       1    2    3    4    5    6    7   8   9
	 * letter   1   0   1    2    3    4    5    6    7   8   9
	 * weight  x1  x9  x8   x7   x6   x5   x4   x3   x2  x1  x1
	 * sum   =  1 + 0 + 8 + 14 + 18 + 20 + 20 + 18 + 14 + 8 + 9 = 130
	 * r     =  130 % 10 = 0 <-- true
	 */

	maps := map[string]string{
		"A": "10", "B": "11", "C": "12", "D": "13", "E": "14",
		"F": "15", "G": "16", "H": "17", "I": "34", "J": "18",
		"K": "19", "L": "20", "M": "21", "N": "22", "O": "35",
		"P": "23", "Q": "24", "R": "25", "S": "26", "T": "27",
		"U": "28", "V": "29", "W": "32", "X": "30", "Y": "31",
		"Z": "33",
	}
	weights := []int{
		1, 9, 8, 7, 6, 5, 4, 3, 2, 1, 1,
	}

	alphabet := idNo[:1] // the first letter.
	digits := idNo[1:]   // the last 9 numbers.

	letters := []rune(maps[alphabet])
	letters = append(letters, []rune(digits)...)

	sum := 0
	l := len(letters)
	for i := 0; i < l; i++ {
		sum += int(letters[i]-'0') * weights[i]
	}
	r := sum % 10

	return r == 0
}

func IsIdNoMale(idNo string) bool {
	if !CheckIdNoDetail(idNo) {
		return false
	}
	genderCode := idNo[1]
	if genderCode == "1"[0] || // Native nationality.
		genderCode == "8"[0] { // Resident certificate.
		return true
	}
	return false
}

func IsIdNoFemale(idNo string) bool {
	if !CheckIdNoDetail(idNo) {
		return false
	}
	genderCode := idNo[1]
	if genderCode == "2"[0] || // Native nationality.
		genderCode == "9"[0] { // Resident certificate.
		return true
	}
	return false
}

func IsIdNoNativeNationality(idNo string) bool {
	if !CheckIdNoDetail(idNo) {
		return false
	}
	genderCode := idNo[1]
	if genderCode == "1"[0] || // Male.
		genderCode == "2"[0] { // Femail.
		return true
	}
	return false
}

func IsIdNoResidentCertificate(idNo string) bool {
	if !CheckIdNoDetail(idNo) {
		return false
	}
	genderCode := idNo[1]
	if genderCode == "8"[0] || // Male.
		genderCode == "9"[0] { // Female.
		return true
	}
	return false
}

func IsIdNoFromNative(idNo string) bool {
	if !CheckIdNoDetail(idNo) {
		return false
	}
	genderCode := idNo[2]
	if genderCode == "0"[0] ||
		genderCode == "1"[0] ||
		genderCode == "2"[0] ||
		genderCode == "3"[0] ||
		genderCode == "4"[0] ||
		genderCode == "5"[0] {
		return true
	}
	return false
}

func IsIdNoFromForeign(idNo string) bool {
	if !CheckIdNoDetail(idNo) {
		return false
	}
	genderCode := idNo[2]
	if genderCode == "6"[0] {
		return true
	}
	return false
}

func IsIdNoFromNonRegistered(idNo string) bool {
	if !CheckIdNoDetail(idNo) {
		return false
	}
	genderCode := idNo[2]
	if genderCode == "7"[0] {
		return true
	}
	return false
}

func IsIdNoFromHongKongOrMacau(idNo string) bool {
	if !CheckIdNoDetail(idNo) {
		return false
	}
	genderCode := idNo[2]
	if genderCode == "8"[0] {
		return true
	}
	return false
}

func IsIdNoFromMainlandChina(idNo string) bool {
	if !CheckIdNoDetail(idNo) {
		return false
	}
	genderCode := idNo[2]
	if genderCode == "9"[0] {
		return true
	}
	return false
}
