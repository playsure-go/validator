package validator_roc

import "regexp"

func CheckMobile(mobile string) bool {
	// 0987654321 or +886987654321
	re := regexp.MustCompile("^(09|\\+8869)[0-9]{8}$")
	if re.MatchString(mobile) {
		return true
	}
	return false
}

func CheckPhone(phone string) bool {
	// 2345678  or 022345678  or +8862345678
	// 23456789 or 0223456789 or +88623456789
	re := regexp.MustCompile("^([1-9][0-9]{6,7}|(0|\\+886)[1-9]{1,2}[0-9]{6,7})$")
	if re.MatchString(phone) {
		return true
	}
	return false
}
