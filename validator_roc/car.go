package validator_roc

import "regexp"

func CheckCarPlateNo(carPlateNo string) bool {
	patterns := []string{
		"^[0-9A-Z]{2,4}-[0-9A-Z]{2,4}$", // AB-12, ABCD-1234
		"^軍[A-Z]-[0-9]{5}$",             // 軍A-90001
		"^臨[A-Z][0-9]{5}$",              // 臨A00001
		"^機臨[0-9]{5}$",                  // 機臨10001
		"^使[0-9]{3}$",                   // 使101
		"^外[0-9]{4}$",                   // 外0001, 外0151
		"^試[0-9]{4,5}$",                 // 試1001, 試10001
		"^試[A-Z][0-9]{4}$",              // 試A1001
		"^機械[A-Z]{2}-[0-9]{2}$",         // 機械ZA-01, 機械MA-01, 機械AA-01
		"^[A-Z]{2}[0-9]{5}$",            // AA50001, AA51888 ( 微型電動二輪車 )
	}
	for _, pattern := range patterns {
		re := regexp.MustCompile(pattern)
		if re.MatchString(carPlateNo) {
			return true
		}
	}
	return false
}
