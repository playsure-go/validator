package validator_roc

import "regexp"

func CheckUbnSimple(ubn string) bool {
	re := regexp.MustCompile("^[0-9]{8}$")
	return re.MatchString(ubn)
}

func CheckUbnDetail(ubn string) bool {
	if !CheckUbnSimple(ubn) {
		return false
	}

	/*
	 *  5   3   2   1   2   5   3   9
	 * x1  x2  x1  x2  x1  x2  x4  x1
	 *  5   6   2   2   2  10  12   9
	 *
	 *                    + 1 + 1
	 *  5 + 6 + 2 + 2 + 2 + 0 + 2 + 9 = 30
	 *  r = 30 % 5 = 0 <-- true
	 *
	 *    Or
	 *
	 *                          *
	 *  5   2   2   1   2   2   7   1
	 * x1  x2  x1  x2  x1  x2  x4  x1
	 *  5   4   2   2   2   4  28   1
	 *
	 *                          2
	 *  5   4   2   2   2   4   8   1 = 30
	 *
	 *                        + 1     = 21 --> X
	 *  5 + 4 + 2 + 2 + 2 + 4 + 0 + 1 = 20 --> O
	 *  r = 20 % 5 = 0 <-- true
	 *
	 *    Or
	 *
	 *                          *
	 *  5   2   1   1   2   2   7   1
	 * x1  x2  x1  x2  x1  x2  x4  x1
	 *  5   4   1   2   2   4  28   1
	 *
	 *                          2
	 *  5   4   1   2   2   4   8   1 = 30
	 *
	 *                        + 1     = 20 --> O
	 *  5 + 4 + 1 + 2 + 2 + 4 + 0 + 1 = 19 --> X
	 *  r = 20 % 5 = 0 <-- true
	 */

	coefficients := []int{1, 2, 1, 2, 1, 2, 4, 1}

	letters := []rune(ubn)

	sum := 0

	l := len(letters)
	for i := 0; i < l; i++ {
		v := int(letters[i]-'0') * coefficients[i]
		sum += v/10 + v%10
	}

	return (sum%5 == 0) || (letters[6] == '7' && (sum+1)%5 == 0)
}
