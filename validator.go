package validator

import "regexp"

func CheckEmail(email string) bool {
	re := regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
	return re.MatchString(email)
}

func CheckSHA1(sha1 string) bool {
	re := regexp.MustCompile("^[a-fA-F0-9]{40}$")
	return re.MatchString(sha1)
}

func CheckSHA256(sha256 string) bool {
	re := regexp.MustCompile("^[a-fA-F0-9]{64}$")
	return re.MatchString(sha256)
}

func CheckMD5(md5 string) bool {
	re := regexp.MustCompile("^[a-fA-F0-9]{32}$")
	return re.MatchString(md5)
}

func CheckUUID(uuid string) bool {
	re := regexp.MustCompile("^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$")
	return re.MatchString(uuid)
}
